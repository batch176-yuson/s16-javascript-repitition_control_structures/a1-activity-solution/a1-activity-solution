let x = parseInt(prompt(`provide a number: `))

console.log(`The number you provided is ${x}.`)

for (x; x >= 0; x--) {
	
	if (x <=50) {
		console.log(`The current value is 50. Terminating the Loop`);
		break;
	}

	if (x % 10 === 0) {
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	}

	if (x % 5 === 0) {
		console.log(x);
	}
}

let word ="supercalifragilisticexpialidocious";
let consonants = " ";

console.log(word);
for(let i = 0; i < word.length; i++){
	if (word[i].toLowerCase() === "a" ||
		word[i].toLowerCase() === "e" ||
		word[i].toLowerCase() === "i" ||
		word[i].toLowerCase() === "o" ||
		word[i].toLowerCase() === "u" ){
		continue;
	} else {
		consonants += word[i];
	}
}
console.log(consonants);
